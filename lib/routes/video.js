'use strict';

const Joi = require('joi');

const timeFormat = /([01][0-9]|[2][0-3]):([01345][0-9]):([01345][0-9])/;

module.exports = (server) => {

    server.route([
        {
            method: 'POST',
            path: '/video/build',
            config: {
                description: 'Video builder.',
                notes: 'Builds a video from an array.',
                tags: ['api'],
                pre: [
                    server.preHandlers.video.fetchStreamUrl,
                    server.preHandlers.video.download,
                    server.preHandlers.video.merge,
                    // server.preHandlers.video.upload,
                    server.preHandlers.video.delete
                ],
                handler: { video: { method: 'build' } },
                validate: {
                    payload: {
                        videos: Joi.array().items(
                            Joi.object().keys({
                                id: Joi.string(),
                                startTime: Joi.string().regex(timeFormat),
                                endTime: Joi.string().regex(timeFormat)
                            })
                        )
                    }
                },
                timeout: {
                    server: 100000
                }
            }
        },
        {
            method: 'GET',
            path: '/video/stream',
            config: {
                description: 'Video streaming.',
                notes: 'Streams a video from a url.',
                tags: ['api'],
                pre: [],
                handler: { video: { method: 'stream' } },
                validate: {
                    query: {
                        url: Joi.string().uri()
                    }
                },
                timeout: {
                    server: 100000
                }
            }
        },
        {
            method: 'POST',
            path: '/video/urls',
            config: {
                description: 'Video urls.',
                notes: 'Fetch video urls from ids.',
                tags: ['api'],
                pre: [
                    server.preHandlers.video.fetchStreamUrl
                ],
                handler: { video: { method: 'urls' } },
                validate: {
                    payload: {
                        videos: Joi.array().items({
                            id: Joi.string()
                        })
                    }
                },
                timeout: {
                    server: 100000
                }
            }
        },
        {
            method: 'GET',
            path: '/video/static',
            config: {
                handler: (request, reply) => {

                    return reply.file(request.query.video);
                }
            }
        }
    ]);
};
