'use strict';

const Joi = require('joi');

module.exports = (server) => {

    server.route([
        {
            method: 'POST',
            path: '/articles/analyze',
            config: {
                description: 'Article analyzer.',
                notes: 'Analyze articles from an array.',
                tags: ['api'],
                pre: [
                    server.preHandlers.analyze.processArticles
                ],
                handler: { analyze: { method: 'articles' } },
                validate: {
                    payload: {
                        articles: Joi.array().items(Joi.string().uri())
                    }
                },
                timeout: {
                    server: 100000
                }
            }
        }
    ]);
};
