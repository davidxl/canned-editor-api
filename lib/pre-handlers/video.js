'use strict';

const Promise = require('bluebird');
const internals = module.exports = {};

internals.fetchStreamUrl = (request, reply) => {

    const searchService = request.server.methods.nbcuVideoSearch;
    searchService
        .getVideoStreams(request.payload.videos)
        .then((urls) => {

            request.payload.videos = request.payload.videos.map((video) => {

                video.url = urls[video.id];
                return video;
            });

            return reply(request.payload.videos);
        });
};

internals.delete = (request, reply) => {

    const deleteService = request.server.methods.deleteFiles;

    Promise.all([
        deleteService.delete('intermediate'),
        deleteService.delete('original')
        // deleteService.delete('ultimate')
    ])
        .then(reply)
        .catch(reply);
};

internals.upload = (request, reply) => {

    const videoService = request.server.methods.video;

    videoService
        .upload(request.pre.merge)
        .then(reply)
        .catch(console.error);
};

internals.merge = (request, reply) => {

    const editorService = request.server.methods.editor;

    editorService.ffmpeg(request.pre.download, (err, data) => {

        if (err) {
            return reply(err);
        }

        return reply({
            url: data
        });
    });
};

internals.download = (request, reply) => {

    const videoService = request.server.methods.video;
    const downloadVideos = [];

    request.payload.videos.map((video) => {

        const dowloadVideo = videoService.download(video);
        downloadVideos.push(dowloadVideo);
    });

    Promise
        .all(downloadVideos)
        .then(reply)
        .catch(reply);
};
