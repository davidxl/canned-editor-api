'use strict';

const Promise = require('bluebird');
const internals = module.exports = {};

let removeDuplicates = (keywords) => {
    let keys = [];

    do {
        let keyword = keywords.shift();

        if (keywords.indexOf(keyword) == -1) {
            keys.push(keyword);
        }

    } while (keywords.length > 0);

    return keys;
};

internals.processArticles = (request, reply) => {

    const analyzeService = request.server.methods.analyze;
    const videoSearch = request.server.methods.nbcuVideoSearch.searchNbcuVideo;
    const processArticles = [];

    request.payload.articles.map((article) => {
        const processAnArticle = analyzeService.analyzeArticle(article);
        processArticles.push(processAnArticle);
    });

    Promise
        .all(processArticles)
        .then((responses) => {
            let keywords = responses
                .map((response) => {
                    return response.keywords.map((keyword) => {
                        return keyword.text;
                    });
                })
                .reduce((list, next) => {
                    return list.concat(next);
                })
                .join(' ')
                .split(' ');

            keywords = removeDuplicates(keywords);

            return videoSearch(keywords.join(' '));
        })
        .then(reply)
        .catch(reply);
};
