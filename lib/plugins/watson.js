'use strict';

const Watson = require('watson-developer-cloud');
const apiKey = {
    api_key: 'd8f6c32ebf4cabb9aa74ff3e7bdc63124a99af06'
};

const internals = {};

internals.register = module.exports = (server, options, next) => {

    // @TODO expose here any other method from watson.
    server.expose('alchemyLanguage', Watson.alchemy_language(apiKey));

    return next();
};

internals.register.attributes = {
    multi: false,
    name: 'hapi-watson',
    version: '1.0.0'
};
