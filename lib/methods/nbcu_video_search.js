'use strict';

const Axios = require('axios');
const ParseString = require('xml2js').parseString;

const internals = module.exports = {};

const instance = Axios.create({
    'baseURL': 'https://api.nbcuni.com:443/news-content',
    'headers': { 'api_key': 'pwx8gdcjjga7fvfpdkqdmraj', 'Accept': 'application/hal+json', 'User-Agent': 'CuembyCannedEditor' }
});

const handleVideoList = (response) => {
    const videos = {};

    response.data._embedded['nbcng:video'].forEach((result) => {
        videos[result.id] = {
            id: result.id,
            title: result.headline,
            description: result.description
        };
    });

    return videos;
};

const retrieveVideoDetails = (video) => {
    const videoUri = '/content/videos/' + video.id;

    return instance.get(videoUri);
};

const parseVideoList = (videos) => {
    const promises = [];

    for (const videoId in videos) {
        promises.push(retrieveVideoDetails(videos[videoId]));
    }

    return Axios.all(promises).then((responses) => {

        responses.forEach((result) => {
            videos[result.data.id].image = result.data._embedded['nbcng:mainImage'].url;

            if (result.data.availableAssets.length == 0) {
                delete videos[result.data.id];
                return;
            }

            let asset;
            if (result.data.availableAssets.length == 1) {
                asset = result.data.availableAssets.pop();
            } else {
                asset = result.data.availableAssets.reduce((value, next) => {
                    if (value.height == 480) {
                        return value;
                    } else {
                        return next;
                    }
                });
            }

            videos[result.data.id].video = asset.url;
        });

        return videos;
    });
};

const getVideoSmiPromise = (url, id) => {
    return Axios.get(url).then(
        (response) => {
            let url = '';

            ParseString(response.data, (err, result) => {
                let obj = result.smil.body.pop().seq.pop();

                if (obj.hasOwnProperty('par')) {
                    obj = obj.par.pop();
                }

                if (obj.hasOwnProperty('switch')) {
                    obj = obj.switch.pop();
                }

                let video = obj.video;

                if (video.length == 1) {
                    video = video.pop();
                } else {
                    video = video.reduce((value, next) => {
                        if (value.$.height == '480') {
                            return value;
                        } else {
                            return next;
                        }
                    });
                }

                url = video.$.src;
            });

            return { id, url };
        }
    );
};

internals.searchNbcuVideo = {
    method: (q) => {
        const searchUri = '/content/videos/search?q=' + q + '&size=25&sort=relevancy,desc';

        return instance.get(searchUri)
            .then(handleVideoList)
            .then(parseVideoList)
            .then((list) => {
                const videos = [];

                for (const videoId in list) {
                    videos.push(list[videoId]);
                }

                return { videos };
            })
            .catch(console.log);
    }
};

internals.getVideoStreams = {
    method: (videos) => {
        const promises = [];

        for (const videoId in videos) {
            promises.push(retrieveVideoDetails(videos[videoId]));
        }

        return Axios.all(promises)
            .then((results) => {
                const urls = {};

                results.forEach((result) => {
                    let asset;
                    if (result.data.availableAssets.length == 1) {
                        asset = result.data.availableAssets.pop();
                    } else {
                        asset = result.data.availableAssets.reduce((value, next) => {
                            if (value.height == 480) {
                                return value;
                            } else {
                                return next;
                            }
                        });
                    }

                    urls[result.data.id] = asset.url;
                });

                return urls;
            })
            .then((urls) => {
                const promises = [];

                for (const videoId in urls) {
                    promises.push(getVideoSmiPromise(urls[videoId], videoId));
                }

                return Axios.all(promises)
                    .then((results) => {
                        const urls = {};

                        results.forEach((result) => {
                            urls[result.id] = result.url;
                        });

                        return urls;
                    });
            })
            .catch(console.log);
    }
};
