'use strict';

const Del = require('delete');
const internals = module.exports = {};

module.exports.delete = {

    method: (subFolder) => {

        return new Promise((resolve, reject) => {

            Del([`${process.cwd()}/files/${subFolder}/*.mp4`], (err) => {

                if (err) {
                    return reject(err);
                }

                console.log('[File Deletion] done!');
                return resolve(subFolder);
            });
        });
    },
    options: {}
};
