'use strict';

const Exec = require('child_process').exec;
const Async = require('async');
const moment = require('moment');
const files = `${process.cwd()}/files`;
let next = 0;

const internals = module.exports = {};

internals.ffmpeg = {

    method: (editingObjects, callback) => {

        Async.each(editingObjects, (object, done) => {

            next++;
            const startTime  = `04/09/2013 ${object.startTime}`;
            const endTime = `04/09/2013 ${object.endTime}`;
            const ms = moment(endTime,"DD/MM/YYYY HH:mm:ss").diff(moment(startTime,"DD/MM/YYYY HH:mm:ss"));
            const d = moment.duration(ms);
            const s = Math.floor(d.asHours()) + moment.utc(ms).format(":mm:ss");
            console.log(`[DURATION]: ${d}`, `string: ${s}`);
            const cmd = `ffmpeg -ss ${object.startTime} -i ${object.localPath} -vcodec libx264 -crf 22 -vb 20M -s 360x288 -t ${s} ${files}/intermediate/output_${next}.mp4`;
            Exec(cmd, { shell: '/bin/bash' }, (error, stdout, stderr) => {

                if (error) {

                    console.log('[EDITOR] err', error);
                    console.log('[EDITOR] stdout', stdout);
                    console.log('[EDITOR] stderr', stderr);
                    return done(error);
                }
                console.log('[EDITOR] Done for this file -> ', object.localPath);
                return done();
            });
        }, (err, done) => {

            if (err) {

                return callback(err);
            }
            next++;
            const finalFile = `${files}/ultimate/output_${next}.mp4`;
            const cmd = `ffmpeg -f concat -safe 0 -i <(for f in ${files}/intermediate/*.mp4; do echo "file '$f'"; done) -vcodec libx264 -crf 22 -s 360x288 -vb 20M -c copy ${finalFile}`;
            Exec(cmd, { shell: '/bin/bash' }, (error, stdout, stderr) => {

                if (error) {

                    console.log('[EDITOR] err', error);
                    console.log('[EDITOR] stdout', stdout);
                    console.log('[EDITOR] stderr', stderr);
                    return callback(error);
                }
                return callback(null , finalFile);
            });
        });
    },
    options: {}
};
