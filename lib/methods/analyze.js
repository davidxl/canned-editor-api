'use strict';

const Promise = require('bluebird');
const Watson = require('watson-developer-cloud');
const internals = module.exports = {};
const apiKey = { api_key: 'd8f6c32ebf4cabb9aa74ff3e7bdc63124a99af06' };
const alchemyLanguage = Watson.alchemy_language(apiKey);

internals.analyzeArticle = {
    method: (url) => {

        // const extract = 'entities,keywords,concepts,doc-emotion,doc-sentiment,typed-rels';
        const extract = 'keywords';
        const parameters = {
            extract,
            url,
            sentiment: 5,
            maxRetrieve: 5
        };

        return new Promise((resolve, reject) => {

            alchemyLanguage.combined(parameters, (error, response) => {

                if (error) {
                    return reject(error);
                }

                return resolve(response);
            });
        });
    },
    options: {}
};
