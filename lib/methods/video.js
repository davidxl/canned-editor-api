'use strict';

const Promise = require('bluebird');
const Axios = require('axios');
const Download = require('download');
const internals = module.exports = {};
const downloadDir = `${process.cwd()}/files/original`;

const auth = (url) => {

    return Axios
        .post(url, {
            auth: {
                identity: {
                    methods: ['password'],
                    password: {
                        user: {
                            name: 'admin_331f703b2c66388a48076c5a091249e1a300ba4e',
                            domain: { name: '1145085' },
                            password: 'stEh4]=2ne&U&TU0'
                        }
                    }
                }
            }
        });
};

internals.upload = {
    method: (video) => {

        return auth('https://identity.open.softlayer.com/v3/auth/tokens')
            .then((response) => {

                const token = response.headers['x-subject-token'];
                const data = require('fs').readFileSync(video);
                const axios = Axios.create({
                    headers: {
                        'X-Auth-Token': token
                    }
                });

                axios
                    .put('https://dal.objectstorage.open.softlayer.com/v1/AUTH_6018b9aa941844b29942fdae3b76d8e7/videos/final.mp4', data);
            });
    },
    options: {}
};

internals.download = {
    method: (video) => {

        const videoUrl = video.url;

        return new Promise((resolve, reject) => {

            if (videoUrl.indexOf('.mp4') !== -1) {
                Download(videoUrl).then((data) => {
                    const fileName = require('url').parse(videoUrl).pathname.split('/').pop();
                    require('fs').writeFileSync(`${downloadDir}/${fileName}`, data);
                    video.localPath = `${downloadDir}/${fileName}`;
                    return resolve(video);
                });
            }

            const axios = Axios.create({
                maxRedirects: 0
            });

            axios
                .get(videoUrl)
                .catch((error) => {

                    const redirectUrl = `${error.response.headers.location}`;
                    Download(redirectUrl).then((data) => {
                        const fileName = require('url').parse(redirectUrl).pathname.split('/').pop();
                        require('fs').writeFileSync(`${downloadDir}/${fileName}`, data);
                        video.localPath = `${downloadDir}/${fileName}`;
                        return resolve(video);
                    });
                });
        });
    },
    options: {}
};
