'use strict';

const Axios = require('axios');

const internals = module.exports = (route, options) => {

    return function (request, reply) {

        return internals[options.method](request, reply);
    };
};

internals.build = (request, reply) => {

    return reply({
        url: require('path').basename(request.pre.merge.url)
    });
};

internals.stream = (request, reply) => {

    Axios
        .get(request.query.url)
        .then((response) => {

            return reply(response.data);
        });
};

internals.urls = (request, reply) => {

    return reply(request.pre.fetchStreamUrl);
};
