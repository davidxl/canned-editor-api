'use strict';

const internals = module.exports = (route, options) => {

    return function (request, reply) {

        return internals[options.method](request, reply);
    };
};

internals.articles = (request, reply) => {

    return reply(request.pre.processArticles);
};